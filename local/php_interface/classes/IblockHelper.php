<?php

use \Bitrix\Main\Loader;
use \Bitrix\Iblock\IblockTable;

class IblockHelper
{
    public static function getIblockIdByCode($code)
    {
        Loader::includeModule('iblock');
        return IblockTable::getRow([
            'filter' => [
                '=CODE' => $code
            ],
            'select' => [
                'ID'
            ]
        ])['ID'];
    }
}