<?php
$MESS['PD_COMP_CL_USER_IS_NOT_EXISTS'] = 'Пользователь с ID #USER_ID# не существует!';
$MESS['PD_COMP_CL_PARENT_NOT_FOUND'] = 'Не найден новый родитель для привязки!';
$MESS['PD_COMP_CL_NO_ORDERS'] = 'Нет заказов';
$MESS['PD_COMP_CL_TABLE_LOGIN'] = 'Логин';
$MESS['PD_COMP_CL_TABLE_FULL_NAME'] = 'ФИО';
$MESS['PD_COMP_CL_TABLE_NAME'] = 'Имя';
$MESS['PD_COMP_CL_TABLE_LAST_NAME'] = 'Фамилия';
$MESS['PD_COMP_CL_TABLE_SECOND_NAME'] = 'Отчество';
$MESS['PD_COMP_CL_TABLE_PERSONAL_PHONE'] = 'Телефон';
$MESS['PD_COMP_CL_TABLE_ORDER_DATE'] = 'Дата последнего заказа';
$MESS['PD_COMP_CL_TABLE_REGISTER_DATE'] = 'Дата регистрации';
$MESS['PD_COMP_CL_TABLE_PARENT_LINK'] = 'Кто пригласил';
$MESS['PD_COMP_CL_EXCEL_SHEET_TITLE'] = 'Деактивация от ';