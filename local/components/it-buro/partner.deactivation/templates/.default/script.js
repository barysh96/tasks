let uiAlert, uiProgress, queryBlocking;

BX.ready(function () {
    BX.bind(BX('start_refresh'), 'click', function() {
        if (!BX.hasClass(this, 'ui-btn-clock')) {
            BX.addClass(this, 'ui-btn-clock');
            refreshDates(1);
        }
    });
});

function refreshDates(page) {
    if (page < 1) {
        page = 1;
    }
    if (page === 1) {
        if (queryBlocking) {
            return;
        } else {
            queryBlocking = true;
        }
    }

    let refreshRequest = BX.ajax.runComponentAction('it-buro:partner.deactivation',
        'datesRefresh', {
            mode: 'class',
            data: {'page': page},
        });
    let refreshResultCallback = function(response) {
        let processing = ajaxProcessing(
            response,
            page,
            BX.message('PD_COMP_CALC_IS_SUCCESS'),
            function() {
                queryBlocking = false;
                BX.removeClass(BX('start_refresh'), 'ui-btn-clock');
                let gridInstance = BX.Main.gridManager.getById(gridId).instance;
                gridInstance.reloadTable('POST');
            }
        );
        if (typeof processing.nextPage === 'number') {
            refreshDates(processing.nextPage);
        }
    };
    refreshRequest.then(refreshResultCallback, refreshResultCallback);
}

function deactivateAction(page = 1) {
    if (page < 1) {
        page = 1;
    }
    if (page === 1) {
        if (queryBlocking) {
            return;
        } else {
            queryBlocking = true;
        }
        BX('progress').scrollIntoView({block: "center", behavior: "smooth"});
        if (!BX.hasClass(BX('apply_button'), 'ui-btn-clock')) {
            BX.addClass(BX('apply_button'), 'ui-btn-clock');
        } else {
            return;
        }
    }

    let gridInstance = BX.Main.gridManager.getById(gridId).instance;
    let values = gridInstance.getActionsPanel().getValues();
    let selectedRows = gridInstance.getRows().getSelectedIds();
    let data = Object.assign({
        ID: selectedRows,
        'page': page
    }, values);

    let deactivateRequest = BX.ajax.runComponentAction('it-buro:partner.deactivation',
        'deactivateUsers', {
            mode: 'class',
            data: {'params': data},
        });
    let deactivateResultCallback = function(response) {
        let processing = ajaxProcessing(
            response,
            page,
            BX.message('PD_COMP_DEACTIVATION_IS_SUCCESS'),
            function() {
                queryBlocking = false;
                BX.removeClass(BX('apply_button'), 'ui-btn-clock');
                gridInstance.reloadTable('POST');
            }
        );
        if (typeof processing.nextPage === 'number') {
            deactivateAction(processing.nextPage);
        }
    };
    deactivateRequest.then(deactivateResultCallback, deactivateResultCallback);
}

function ajaxProcessing(response, page, successMessage, stopCallback)
{
    if (typeof uiAlert === 'undefined') {
        uiAlert = new BX.UI.Alert({
            closeBtn: true,
        });
    }
    if (typeof uiProgress === 'undefined') {
        uiProgress = new BX.UI.ProgressBar({
            fill: true,
            color: BX.UI.ProgressBar.Color.PRIMARY,
            maxValue: 1,
            value: 0,
            statusType: BX.UI.ProgressBar.Status.PERCENT,
        });
    }

    let resultAnswer = {};
    let result;

    let errorMessage = false;
    if (response.status === 'error') {
        errorMessage = BX.message('PD_COMP_SIMPLE_ERROR');
        for (let i in response.errors) {
            errorMessage += '<br>' + response.errors[i].message;
        }
    } else if (response.data == null) {
        errorMessage = BX.message('PD_COMP_SIMPLE_ERROR');
    } else if (typeof response.data.error === 'string') {
        errorMessage = response.data.error;
    } else if (typeof response.data.all === 'undefined' || typeof response.data.processed === 'undefined') {
        errorMessage = BX.message('PD_COMP_UNKNOWN_ANSWER');
    }
    if (errorMessage) {
        uiAlert.setText(errorMessage);
        uiAlert.setColor(BX.UI.Alert.Color.DANGER);
        uiAlert.setIcon(BX.UI.Alert.Icon.DANGER);
        result = uiAlert;
        if (typeof stopCallback === 'function') {
            stopCallback.call(this);
        }
    } else {
        if (response.data.all > response.data.processed) {
            uiProgress.setMaxValue(response.data.all);
            uiProgress.update(response.data.processed);
            result = uiProgress;
            resultAnswer.nextPage = ++page;
        } else {
            uiAlert.setText(successMessage);
            uiAlert.setColor(BX.UI.Alert.Color.SUCCESS);
            uiAlert.setIcon(BX.UI.Alert.Icon.INFO);
            result = uiAlert;
            if (typeof stopCallback === 'function') {
                stopCallback.call(this);
            }
        }
    }
    if (typeof result === 'object') {
        BX.cleanNode(BX('progress'));
        BX.append(result.getContainer(), BX('progress'));
    }

    return resultAnswer;
}