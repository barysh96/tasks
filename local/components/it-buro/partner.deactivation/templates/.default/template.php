<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$this->addExternalCSS('/bitrix/css/main/grid/webform-button.css');

\Bitrix\Main\UI\Extension::load("ui.progressbar");
\Bitrix\Main\UI\Extension::load("ui.buttons");
\Bitrix\Main\UI\Extension::load("ui.buttons.icons");
\Bitrix\Main\UI\Extension::load("ui.alerts");
?>
<script>
    var gridId = '<?= $arParams['GRID_LIST_ID'] ?>';
    BX.message({
        PD_COMP_CALC_IS_SUCCESS:'<?= Loc::getMessage('PD_COMP_CALC_IS_SUCCESS') ?>',
        PD_COMP_DEACTIVATION_IS_SUCCESS:'<?= Loc::getMessage('PD_COMP_DEACTIVATION_IS_SUCCESS') ?>',
        PD_COMP_SIMPLE_ERROR:'<?= Loc::getMessage('PD_COMP_SIMPLE_ERROR') ?>',
        PD_COMP_UNKNOWN_ANSWER:'<?= Loc::getMessage('PD_COMP_UNKNOWN_ANSWER') ?>'
    });
</script>
<div class="adm-toolbar-panel-container">
    <div class="adm-toolbar-panel-flexible-space">
        <?php
        $APPLICATION->IncludeComponent('bitrix:main.ui.filter', '', [
            'FILTER_ID' => $arParams['GRID_FILTER_ID'],
            'GRID_ID' => $arParams['GRID_LIST_ID'],
            'FILTER' => $arResult['UI_FILTER'],
            'ENABLE_LIVE_SEARCH' => true,
            'ENABLE_LABEL' => true
        ]);
        ?>
    </div>
    <div class="adm-toolbar-panel-align-right">
        <div class="ui-btn-container">
            <?if ($arResult['EXCEL_URI']):?>
                <button class="ui-btn ui-btn-light-border ui-btn-icon-setting" onclick="
                        BX.adminList.ShowMenu(this, [{'LINK': '<?= $arResult['EXCEL_URI'] ?>', 'GLOBAL_ICON':'adm-menu-excel', 'TEXT':'Excel','TITLE':'<?= Loc::getMessage('PD_COMP_EXCEL_DESCRIPTION') ?>'}]);"></button>
            <?endif;?>
            <button class="ui-btn ui-btn-primary"
                   id="start_refresh" title="<?= Loc::getMessage('PD_COMP_PD_COMP_RECOUNT_DESCRIPTION') ?>">
                <?= Loc::getMessage('PD_COMP_RECOUNT_BUTTON') ?>
            </button>
        </div>
    </div>
</div>
<div class="ui-btn-container">
    <div id="progress"></div>
</div>
<?php
$tableData = [];
foreach ($arResult['USERS'] as $arItem):
    $rowData = [];
    foreach ($arResult['COLUMNS'] as $column) {
        $rowData[$column['id']] = $arItem[$column['id']];
    }
    $arActions = [
        [
            'TEXT' => Loc::getMessage('PD_COMP_PD_ACTION_LOOK'),
            'ONCLICK' => 'window.open(\'/bitrix/admin/user_edit.php?ID=' . $arItem['ID'] . '\',\'_blank\');'
        ]
    ];
    if ($arItem['UF_PARENT'] > 0) {
        $arActions[] = [
            'TEXT' => Loc::getMessage('PD_COMP_PD_ACTION_LOOK_PARENT'),
            'ONCLICK' => 'window.open(\'/bitrix/admin/user_edit.php?ID=' . $arItem['UF_PARENT'] . '\',\'_blank\');'
        ];
    }

    $tableData[] = [
        'id' => $arItem['ID'],
        'actions' => $arActions,
        'data' => $rowData
    ];
endforeach;

$snippet = new \Bitrix\Main\Grid\Panel\Snippet();

$APPLICATION->IncludeComponent('bitrix:main.ui.grid', '', [
    'GRID_ID' => $arParams['GRID_LIST_ID'],
    'COLUMNS' => $arResult['COLUMNS'],
    'ROWS' => $tableData,
    'SHOW_ROW_CHECKBOXES' => true,
    'NAV_OBJECT' => $arResult['NAV'],
    'AJAX_MODE' => 'Y',
    'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', ''),
    'PAGE_SIZES' => [
        ['NAME' => '10', 'VALUE' => '10'],
        ['NAME' => '20', 'VALUE' => '20'],
        ['NAME' => '50', 'VALUE' => '50'],
        ['NAME' => '100', 'VALUE' => '100']
    ],
    'AJAX_OPTION_JUMP' => 'Y',
    'SHOW_CHECK_ALL_CHECKBOXES' => true,
    'SHOW_ROW_ACTIONS_MENU' => true,
    'SHOW_GRID_SETTINGS_MENU' => true,
    'SHOW_NAVIGATION_PANEL' => true,
    'SHOW_PAGINATION' => true,
    'SHOW_SELECTED_COUNTER' => true,
    'TOTAL_ROWS_COUNT' => $arResult['NAV']->getRecordCount(),
    'SHOW_TOTAL_COUNTER' => true,
    'SHOW_PAGESIZE' => true,
    'SHOW_ACTION_PANEL' => true,
    'ACTION_PANEL' => [
        'GROUPS' => [
            'TYPE' => [
                'ITEMS' => [
                    [
                        'TYPE' => \Bitrix\Main\Grid\Panel\Types::TEXT,
                        'ID' => 'NEW_PARENT',
                        'NAME' => 'NEW_PARENT',
                        'VALUE' => '1'
                    ],
                    $snippet->getApplyButton([
                        'ONCHANGE' => [
                            [
                                'ACTION' => \Bitrix\Main\Grid\Panel\Actions::CALLBACK,
                                'DATA' => [
                                    [
                                        'JS' => 'deactivateAction();'
                                    ]
                                ]
                            ]
                        ]
                    ]),
                    $snippet->getForAllCheckbox(),
                ],
            ]
        ],
    ],
    'ALLOW_COLUMNS_SORT' => true,
    'ALLOW_COLUMNS_RESIZE' => true,
    'ALLOW_HORIZONTAL_SCROLL' => true,
    'ALLOW_SORT' => true,
    'ALLOW_PIN_HEADER' => true,
    'AJAX_OPTION_HISTORY' => 'N'
],
    $this->getComponent(),
    ['HIDE_ICONS' => 'Y']
);