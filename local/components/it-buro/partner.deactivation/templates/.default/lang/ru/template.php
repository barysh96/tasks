<?php
$MESS['PD_COMP_CALC_IS_SUCCESS'] = 'Перерасчет успешно завершен!';
$MESS['PD_COMP_DEACTIVATION_IS_SUCCESS'] = 'Деактивация завершена успешно!';
$MESS['PD_COMP_SIMPLE_ERROR'] = 'Произошла ошибка.';
$MESS['PD_COMP_UNKNOWN_ANSWER'] = 'Не удается обработать ответ сервера.';
$MESS['PD_COMP_EXCEL_DESCRIPTION'] = 'Выгрузить данные из списка в Excel';
$MESS['PD_COMP_RECOUNT_BUTTON'] = 'Пересчитать даты заказов';
$MESS['PD_COMP_PD_COMP_RECOUNT_DESCRIPTION'] = 'Запустить пересчет даты последнего заказа у пользователей';
$MESS['PD_COMP_PD_ACTION_LOOK'] = 'Посмотреть';
$MESS['PD_COMP_PD_ACTION_LOOK_PARENT'] = 'Посмотреть пригласившего';