<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = [
    'NAME' => 'Деактивация неактивных партнеров',
    'DESCRIPTION' => 'Выводит Список пользователей, чтобы кладовщик мог авторизоватья им и оформить заказ',
    'PATH' => [
        'ID' => 'it-buro',
        'NAME' => 'IT-BURO',
        'SORT' => 10
    ],
];
?>