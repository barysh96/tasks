<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\UserTable;
use Bitrix\Main\Grid\Options as GridOptions;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Web\Uri;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

Loader::includeModule('sale');

class PartnersDeactivator extends \CBitrixComponent implements Controllerable
{
    public function configureActions()
    {
        return [];
    }

    public function datesRefreshAction($page)
    {
//        $filter['ACTIVE'] = 'Y';
        $filter[] = [
            'LOGIC' => 'OR',
            ['=ACTIVE' => 'Y'],
            ['=UF_PREV_PARENT_UNTIE' => false, '!UF_PARENT' => false]
        ];
        $filter['!@ID'] = $this->getGroupFilterQuery([1, \ItBuro\Config::STOCK_USER_GROUP_ID, \ItBuro\Config::SYSTEM_USERS_GROUP_ID]);

        $nav = new PageNavigation('refresh_order_date_nav');
        $nav->allowAllRecords(false)
            ->setPageSize(40)
            ->setCurrentPage($page);
        $rsUsers = UserTable::getList([
            'filter' => $filter,
            'order' => ['ID' => 'ASC'],
            'select' => [
                'ID', 'UF_LAST_ORDER_DATE', 'DATE_REGISTER'
            ],
            'limit' => $nav->getLimit(),
            'offset' => $nav->getOffset(),
            'count_total' => true,
        ]);
        //configure nav object
        $nav->setRecordCount($rsUsers->getCount());
        $cUser = new \CUser();
        while ($arUser = $rsUsers->fetch()) {
            $arOrder = \Bitrix\Sale\Internals\OrderTable::getRow([
                'filter' => [
                    'PAYED' => 'Y',
                    'USER_ID' => $arUser['ID']
                ],
                'order' => ['DATE_INSERT' => 'DESC'],
                'select' => ['DATE_INSERT']
            ]);
            $orderDate = empty($arOrder) ? $arUser['DATE_REGISTER'] : $arOrder['DATE_INSERT'];
            $curOrderDate = empty($arUser['UF_LAST_ORDER_DATE']) ? false : $arUser['UF_LAST_ORDER_DATE'];
            if ($orderDate != $curOrderDate) {
                if (is_object($orderDate)) {
                    $orderDate = ConvertTimeStamp($orderDate->getTimestamp(), 'FULL');
                }
                $cUser->Update($arUser['ID'], [
                    'UF_LAST_ORDER_DATE' => $orderDate
                ]);
            }
        }

        $allCount = $nav->getRecordCount();
        $processedCount = $nav->getOffset() + $rsUsers->getSelectedRowsCount();
        return [
            'all' => $allCount,
            'processed' => $processedCount
        ];
    }

    public function deactivateUsersAction($params)
    {
        $result = [
            'all' => 0,
            'processed' => 0
        ];
        $newParent = $params['NEW_PARENT'];
        if (!empty($newParent)) {
            if (!empty(
                UserTable::getRow([
                    'filter' => ['ID' => $newParent],
                    'select' => ['ID']
                ])
            )) {
                $allRows = $params['action_all_rows_' . $this->arParams['GRID_LIST_ID']] == 'Y';
                $page =  1;
                $nav = new PageNavigation('refresh_order_date_nav');
                $nav->allowAllRecords(false)
                    ->setPageSize(10)
                    ->setCurrentPage($page);
                if ($allRows) {
                    $filter = $this->getFilter();
                } else {
                    $filter = ['ID' => $params['ID']];
                }
                $rsUsers = UserTable::getList([
                    'filter' => $filter,
                    'order' => ['ID' => 'ASC'],
                    'select' => ['ID', 'UF_PARENT'],
                    'limit' => $nav->getLimit(),
                    'offset' => $nav->getOffset(),
                    'count_total' => true,
                ]);
                //configure nav object
                $nav->setRecordCount($rsUsers->getCount());

                $cUser = new \CUser();
                $arUsers = [];
                while ($user = $rsUsers->fetch()) {
                    $arUsers[$user['ID']] = $user;
                }

                // Ставим все детям в родителя - родителя родителя)
                $rsChildren = UserTable::getList([
                    'filter' => [
                        '@UF_PARENT' => array_keys($arUsers),
                        '!@ID' => array_keys($arUsers)
                    ],
                    'select' => ['ID', 'UF_PARENT'],
                ]);
                while ($child = $rsChildren->fetch()) {
                    $updateParent = $arUsers[$child['UF_PARENT']]['UF_PARENT'];
                    while (array_key_exists($updateParent, $arUsers)) { // Обрабатываем ситуацию, когда деактивируется родитель и его ребенок
                        $updateParent = $arUsers[$updateParent]['UF_PARENT'];
                    }
                    $cUser->Update($child['ID'], ['UF_PARENT' => $updateParent, 'UF_PREV_PARENT' => $child['UF_PARENT']]);
                }

                // Деактивируем и перепривязваем пользователей
                foreach ($arUsers as $userId => $userData) {
                    $cUser->Update(
                        $userId,
                        ['ACTIVE' => 'N', 'UF_PARENT' => $newParent, 'UF_PREV_PARENT_UNTIE' => $userData['UF_PARENT']]
                    );
                }


                $offset = ($params['page'] - 1) * $nav->getPageSize();
                $result['all'] = $offset + $nav->getRecordCount();
                $result['processed'] = $offset + $rsUsers->getSelectedRowsCount();
            } else {
                $result['error'] = Loc::getMessage('PD_COMP_CL_USER_IS_NOT_EXISTS', ['#USER_ID#' => $newParent]);
            }

        } else {
            $result['error'] = Loc::getMessage('PD_COMP_CL_PARENT_NOT_FOUND');
        }
        return $result;
    }

    public function onPrepareComponentParams($arParams)
    {
        $arParams['EXCEL'] = $this->request->get('mode') === 'excel';
        $arParams['GRID_FILTER_ID'] = 'DEACTIVATION_GRID_FILTER';
        $arParams['GRID_LIST_ID'] = 'DEACTIVATION_GRID_TABLE';
        return $arParams;
    }

    private function checkPermission()
    {
        if (!$GLOBALS['USER']->IsAdmin()) {
            throw new \Bitrix\Main\AccessDeniedException();
        }
    }

    protected function getFilter()
    {
        $filter = [];
        $filterOption = new Bitrix\Main\UI\Filter\Options($this->arParams['GRID_FILTER_ID']);
        $filterGrid = $filterOption->getFilter([]);
        if (!empty($filterGrid)) {
            //ID
            switch ($filterGrid['ID_numsel']) {
                case 'exact':
                    $filter['=ID'] = $filterGrid['ID_from'];
                    break;
                case 'range':
                    $filter['><ID'] = [$filterGrid['ID_from'], $filterGrid['ID_to']];
                    break;
                case 'more':
                    $filter['>ID'] = $filterGrid['ID_from'];
                    break;
                case 'less':
                    $filter['<ID'] = $filterGrid['ID_to'];
                    break;
            }
            //Login
            if (!empty($filterGrid['LOGIN'])) {
                $filter['LOGIN'] = $filterGrid['LOGIN'];
            }
            //Name
            if (!empty($filterGrid['NAME'])) {
                $filter['NAME'] = $filterGrid['NAME'];
            }
            //Last Name
            if (!empty($filterGrid['LAST_NAME'])) {
                $filter['LAST_NAME'] = $filterGrid['LAST_NAME'];
            }
            //Second Name
            if (!empty($filterGrid['SECOND_NAME'])) {
                $filter['SECOND_NAME'] = $filterGrid['SECOND_NAME'];
            }

            //Date register
            if (!empty($filterGrid['DATE_REGISTER_from'])) {
                $filter['>=DATE_REGISTER'] = $filterGrid['DATE_REGISTER_from'];
            }
            if (!empty($filterGrid['DDATE_REGISTER_to'])) {
                $filter['<=DATE_REGISTER'] = $filterGrid['DATE_REGISTER_to'];
            }
            //Last order Date
            if (!empty($filterGrid['LAST_ORDER_DATE_from'])) {
                $filter['>=UF_LAST_ORDER_DATE'] = $filterGrid['LAST_ORDER_DATE_from'];
            }
            if (!empty($filterGrid['LAST_ORDER_DATE_to'])) {
                $filter['<=UF_LAST_ORDER_DATE'] = $filterGrid['LAST_ORDER_DATE_to'];
            }

            //Search
            if (!empty($find = trim($filterGrid['FIND']))) {
                $findFilter = ['LOGIC' => 'AND'];
                foreach (explode(' ', $find, 3) as $word) {
                    $findFilter[] = [
                        'LOGIC' => 'OR',
                        ['LOGIN' => '%' . $word . '%', '!LOGIN' => false],
                        ['NAME' => '%' . $word . '%', '!NAME' => false],
                        ['LAST_NAME' => '%' . $word . '%', '!LAST_NAME' => false],
                        ['SECOND_NAME' => '%' . $word . '%', '!SECOND_NAME' => false]
                    ];
                }
                $filter[] = $findFilter;
            }

        }
//        $filter['ACTIVE'] = 'Y'; //появилась проблема ранее деактивированных, но не отвязанных.
        $filter[] = [
            'LOGIC' => 'OR',
            ['=ACTIVE' => 'Y'],
            ['=UF_PREV_PARENT_UNTIE' => false, '!UF_PARENT' => false]
        ];


        $date = new \Bitrix\Main\Type\DateTime();
        $date->add('-1 year');
        $filter[] = [
            'LOGIC' => 'OR',
            ['<UF_LAST_ORDER_DATE' => $date],
            ['UF_LAST_ORDER_DATE' => false]
        ];
        $filter['!@ID'] = $this->getGroupFilterQuery([1, \ItBuro\Config::STOCK_USER_GROUP_ID, \ItBuro\Config::SYSTEM_USERS_GROUP_ID]);
        return $filter;
    }

    protected function getGroupFilterQuery($group)
    {
        $connection = \Bitrix\Main\Application::getConnection();
        $helper = $connection->getSqlHelper();

        $query = new \Bitrix\Main\Entity\Query(Bitrix\Main\UserGroupTable::getEntity());
        $query->setSelect(['D_USER_ID']);
        $query->setFilter([
            'GROUP_ID' => $group,
            [
                'LOGIC' => 'OR',
                'DATE_ACTIVE_FROM' => false,
                '<=DATE_ACTIVE_FROM' => new \Bitrix\Main\DB\SqlExpression($helper->getCurrentDateTimeFunction())
            ],
            [
                'LOGIC' => 'OR',
                'DATE_ACTIVE_TO' => false,
                '>=DATE_ACTIVE_TO' => new \Bitrix\Main\DB\SqlExpression($helper->getCurrentDateTimeFunction())
            ]
        ]);
        $query->registerRuntimeField(0, new \Bitrix\Main\Entity\ExpressionField('D_USER_ID', 'DISTINCT(USER_ID)'));


//        return $query->getQuery();
        return array_column($query->exec()->fetchAll(), 'D_USER_ID');
    }

    protected function getClients()
    {
        $grid_options = new GridOptions($this->arParams['GRID_LIST_ID']);
        //form filter
        $filter = $this->getFilter();
        //form sort
        $gridSort = $grid_options->GetSorting(['sort' => ['ID' => 'ASC'], 'vars' => ['by' => 'by', 'order' => 'order']]);
        $sort = !empty($gridSort['sort']) ? $gridSort['sort'] : ['ID' => 'ASC'];
        //collect params
        $partnersParams = [
            'filter' => $filter,
            'order' => $sort,
            'select' => [
                'ID', 'LOGIN', 'LAST_NAME', 'NAME', 'SECOND_NAME', 'UF_LAST_ORDER_DATE', 'DATE_REGISTER', 'UF_PARENT', 'PERSONAL_PHONE'
            ]
        ];

        if (!$this->arParams['EXCEL']) {
            //form pagination
            $nav_params = $grid_options->GetNavParams();
            $nav = new PageNavigation($this->arParams['GRID_LIST_ID']);
            $nav->allowAllRecords(true)
                ->setPageSize($nav_params['nPageSize'])
                ->initFromUri();

            $partnersParams['limit'] = $nav->getLimit();
            $partnersParams['offset'] = $nav->getOffset();
            $partnersParams['count_total'] = true;
        }
        //get query
        $rsUsers = UserTable::getList($partnersParams);
        if (isset($nav)) {
            //configure nav object
            $nav->setRecordCount($rsUsers->getCount());
            $this->arResult['NAV'] = $nav;
        }
        //fetch data
        $this->arResult['USERS'] = [];
        while ($arUser = $rsUsers->fetch()) {
            $arUser['FULL_NAME'] = \CUser::FormatName(
//                CSite::GetNameFormat(true),
                '#NAME# #SECOND_NAME# #LAST_NAME#',
                $arUser,
                false,
                true
            );
            $arUser['ORDER_DATE'] = !empty($arUser['UF_LAST_ORDER_DATE']) ? $arUser['UF_LAST_ORDER_DATE']->format('d.m.Y') : Loc::getMessage('PD_COMP_CL_NO_ORDERS');
            $arUser['REGISTER_DATE'] = !empty($arUser['DATE_REGISTER']) ? $arUser['DATE_REGISTER']->format('d.m.Y') : '-';
            $arUser['ID_LINK'] = $this->arParams['EXCEL']
                ? $arUser['ID']
                : '<a href=\'/bitrix/admin/user_edit.php?ID=' . $arUser['ID'] . '\' target=\'_blank\'>'. $arUser['ID'] . '</a>';
            if ($arUser['UF_PARENT'] > 0) {
                $arUser['PARENT_LINK'] = $this->arParams['EXCEL']
                    ?  $arUser['UF_PARENT']
                    : '<a href=\'/bitrix/admin/user_edit.php?ID=' . $arUser['UF_PARENT'] . '\' target=\'_blank\'>'. $arUser['UF_PARENT'] . '</a>';
            }
            $this->arResult['USERS'][$arUser['ID']] = $arUser;
        }
    }

    protected function getColumns()
    {
        $this->arResult['COLUMNS'] = [
            ['id' => 'ID_LINK', 'name' => 'ID', 'sort' => 'ID', 'default' => true],
            ['id' => 'LOGIN', 'name' => Loc::getMessage('PD_COMP_CL_TABLE_LOGIN'), 'sort' => 'LOGIN', 'default' => true],
            ['id' => 'FULL_NAME', 'name' => Loc::getMessage('PD_COMP_CL_TABLE_FULL_NAME'), 'sort' => 'NAME', 'default' => true],
            ['id' => 'PERSONAL_PHONE', 'name' => Loc::getMessage('PD_COMP_CL_TABLE_PERSONAL_PHONE'), 'sort' => 'PERSONAL_PHONE', 'default' => true],
            ['id' => 'ORDER_DATE', 'name' => Loc::getMessage('PD_COMP_CL_TABLE_ORDER_DATE'), 'sort' => 'UF_LAST_ORDER_DATE', 'default' => true],
            ['id' => 'REGISTER_DATE', 'name' => Loc::getMessage('PD_COMP_CL_TABLE_REGISTER_DATE'), 'sort' => 'DATE_REGISTER', 'default' => true],
            ['id' => 'PARENT_LINK', 'name' => Loc::getMessage('PD_COMP_CL_TABLE_PARENT_LINK'), 'sort' => 'UF_PARENT', 'default' => true],
        ];
    }

    protected function getLetter($number)
    {
        $char = '';
        for ($i = 0; $i < floor($number / 26); $i ++) {
            $char .= chr(833 + 25);
        }
        $char .= chr(833 + ($number % 26));
        return $char;
    }

    protected function excelOutput()
    {
        $GLOBALS['APPLICATION']->RestartBuffer();
        $grid_options = new GridOptions($this->arParams['GRID_LIST_ID']);
        $currentOptions = $grid_options->getCurrentOptions();
        if (!empty($currentOptions['columns'])) {
            $visibleColumns = explode(',', $currentOptions['columns']);
            if (!empty($visibleColumns)) {
                foreach ($this->arResult['COLUMNS'] as $k => $column) {
                    if (!in_array($column['id'], $visibleColumns)) {
                        unset($this->arResult['COLUMNS'][$k]);
                    }
                }
            }
        }
        //generate and download excel file
        $spreadSheet = new Spreadsheet();
        $sheet = $spreadSheet->getActiveSheet();
        $row = 1;
        $sheet->setTitle(Loc::getMessage('PD_COMP_CL_EXCEL_SHEET_TITLE') . date('d.m.Y H-i'));
        $columnLetters = [];
        //заголовки
        foreach ($this->arResult['COLUMNS'] as $col => $data) {
            $columnLetters[$col] = $this->getLetter(count($columnLetters));
            $sheet->getColumnDimension($columnLetters[$col])->setAutoSize(true);
            $sheet->getCell($columnLetters[$col] . $row)->setValue($data['name']);
        }
        //Основная часть
        foreach ($this->arResult['USERS'] as $arItem):
            $row++;
            foreach ($this->arResult['COLUMNS'] as $col => $column) {
                $sheet->getCell("{$columnLetters[$col]}{$row}")->setValue($arItem[$column['id']]);
            }
        endforeach;

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="deactivate_unactive' . time() .'.xlsx"');
        header('Cache-Control: max-age=0');

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadSheet, 'Xlsx');

        $writer->save('php://output');
        die;
    }

    public function executeComponent()
    {
        try {
            $this->checkPermission();
            $this->getClients();
            $this->getColumns();
            if ($this->arParams['EXCEL']) {
                $this->excelOutput();
            } else {
                $uriString = $this->request->getRequestUri();
                $uri = new Uri($uriString);
                $uri->addParams(['mode' => 'excel']);
                $this->arResult['EXCEL_URI'] = $uri->getUri();

                $this->arResult['UI_FILTER'] = [
                    ['id' => 'ID', 'name' => 'ID', 'type'=>'number', 'default' => true],
                    ['id' => 'LOGIN', 'name' => Loc::getMessage('PD_COMP_CL_TABLE_LOGIN'), 'type'=>'string', 'default' => false],
                    ['id' => 'NAME', 'name' => Loc::getMessage('PD_COMP_CL_TABLE_NAME'), 'type'=>'string', 'default' => false],
                    ['id' => 'LAST_NAME', 'name' => Loc::getMessage('PD_COMP_CL_TABLE_LAST_NAME'), 'type'=>'string', 'default' => false],
                    ['id' => 'SECOND_NAME', 'name' => Loc::getMessage('PD_COMP_CL_TABLE_SECOND_NAME'), 'type'=>'string', 'default' => false],
                    ['id' => 'DATE_REGISTER', 'name' => Loc::getMessage('PD_COMP_CL_TABLE_REGISTER_DATE'), 'type'=>'date', 'default' => false],
                    ['id' => 'LAST_ORDER_DATE', 'name' => Loc::getMessage('PD_COMP_CL_TABLE_ORDER_DATE'), 'type'=>'date', 'default' => false]
                ];
                $this->includeComponentTemplate();
            }
        } catch (Exception $error) {
            ShowError($error->getMessage());
        }
    }
}