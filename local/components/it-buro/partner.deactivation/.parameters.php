<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

try {
    $arComponentParameters = array(
        'GROUPS' => array(),
        'PARAMETERS' => array(
            'FILTER_NAME' =>  array(
                'PARENT'  => 'BASE',
                'NAME'    => "Имя фильтра",
                'TYPE'    => 'STRING',
                'DEFAULT' => '0'
            )
        )
    );
}
catch (Main\LoaderException $e) {
    ShowError($e->getMessage());
}
?>