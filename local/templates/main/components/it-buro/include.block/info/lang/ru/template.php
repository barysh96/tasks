<?php

$MESS['VIEW_MAP'] = 'Посмотреть на карте';
$MESS['SIGN_UP'] = 'Записаться на консультацию';
$MESS['INFO_TERRITORY_MES'] = 'Территория комплекса';
$MESS['INFO_RENT_MES'] = 'Нежилые помещения арендуемые';
$MESS['INFO_CARS_MES'] = 'Количество машиномест';
$MESS['INFO_APARTMENT_MES'] = 'Количество квартир';
$MESS['INFO_AREA_MES'] = 'Площадь жилого фонда';
$MESS['INFO_PEOPLE_MES'] = 'Количество жителей';
$MESS['METERS'] = 'м';
$MESS['THING'] = 'шт';
$MESS['HECTARE'] = 'Га';
$MESS['HUMANS'] = 'чел';