<?php

$MESS['INFO_PRE_TITLE'] = 'Пред-заголовок';
$MESS['INFO_TITLE'] = 'Заголовок';
$MESS['INFO_LINK_TEXT'] = 'Текст ссылки';
$MESS['INFO_LINK_HREF'] = 'Адрес ссылки';
$MESS['INFO_ADDRESS'] = 'Адрес';
$MESS['INFO_TERRITORY'] = 'Территория комплекса, Га';
$MESS['INFO_RENT'] = 'Нежилые помещения арендуемые, м2';
$MESS['INFO_CARS'] = 'Количество машиномест, шт.';
$MESS['INFO_APARTMENT'] = 'Количество квартир, шт.';
$MESS['INFO_AREA'] = 'Площадь жилого фонда, м2';
$MESS['INFO_PEOPLE'] = 'Количество жителей, чел.';