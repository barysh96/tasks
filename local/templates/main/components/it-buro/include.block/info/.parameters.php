<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

$arTemplateParameters = [
    'PRE_TITLE' => [
        'NAME' => Loc::getMessage('INFO_PRE_TITLE'),
        'TYPE' => 'STRING',
    ],
    'TITLE' => [
        'NAME' => Loc::getMessage('INFO_TITLE'),
        'TYPE' => 'STRING',
    ],
    'LINK_TEXT' => [
        'NAME' => Loc::getMessage('INFO_LINK_TEXT'),
        'TYPE' => 'STRING',
    ],
    'LINK_HREF' => [
        'NAME' => Loc::getMessage('INFO_LINK_HREF'),
        'TYPE' => 'STRING',
    ],
    'ADDRESS' => [
        'NAME' => Loc::getMessage('INFO_ADDRESS'),
        'TYPE' => 'STRING',
    ],
    'TERRITORY' => [
        'NAME' => Loc::getMessage('INFO_TERRITORY'),
        'TYPE' => 'STRING',
    ],
    'RENT' => [
        'NAME' => Loc::getMessage('INFO_RENT'),
        'TYPE' => 'STRING',
    ],
    'CARS' => [
        'NAME' => Loc::getMessage('INFO_CARS'),
        'TYPE' => 'STRING',
    ],
    'APARTMENT' => [
        'NAME' => Loc::getMessage('INFO_APARTMENT'),
        'TYPE' => 'STRING',
    ],
    'AREA' => [
        'NAME' => Loc::getMessage('INFO_AREA'),
        'TYPE' => 'STRING',
    ],
    'PEOPLE' => [
        'NAME' => Loc::getMessage('INFO_PEOPLE'),
        'TYPE' => 'STRING',
    ],
];
