<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

use \Bitrix\Main\Localization\Loc;
?>
<div class="info">
    <div class="container info__container">
        <div class="info__pre-title"><?=$arParams['PRE_TITLE']?></div>
        <div class="title-wrapper d-flex flex-column flex-md-row">
            <div class="title info__title"><?=$arParams['TITLE']?></div>
            <a class="info__sell d-flex align-items-center" href="<?=$arParams['LINK_HREF']?>" target="_blank">
                <?=$arParams['LINK_TEXT']?>
                <img class="info__sell-ico" src="<?=SITE_TEMPLATE_PATH?>/img/ico-7.svg" alt="">
            </a>
        </div>
        <div class="info__adress"><?=$arParams['ADDRESS']?></div>
        <div class="look-map d-none d-xl-flex align-items-center" href="#block-map">
            <img class="ico-map" src="<?=SITE_TEMPLATE_PATH?>/img/ico-map.svg" alt="">
            <?=Loc::getMessage('VIEW_MAP')?>
        </div>
        <div class="info__list row">
            <div class="col-6 col-xl-3 info__item d-flex d-md-block flex-column flex-md-row"><img class="ico" src="<?=SITE_TEMPLATE_PATH?>/img/ico-1.svg" alt="">
                <div class="d-flex flex-column"><span class="info__nums"><?=$arParams['TERRITORY']?> <?=Loc::getMessage('HECTARE')?></span><span class="info__desc"><?=Loc::getMessage('INFO_TERRITORY_MES')?></span></div>
            </div>
            <div class="col-6 col-xl-3 info__item d-flex d-md-block flex-column flex-md-row"><img class="ico" src="<?=SITE_TEMPLATE_PATH?>/img/ico-2.svg" alt="">
                <div class="d-flex flex-column"><span class="info__nums"><?=$arParams['RENT']?> <?=Loc::getMessage('METERS')?><sup>2</sup></span><span class="info__desc"><?=Loc::getMessage('INFO_RENT_MES')?></span></div>
            </div>
            <div class="col-6 col-xl-3 info__item d-flex d-md-block flex-column flex-md-row"><img class="ico" src="<?=SITE_TEMPLATE_PATH?>/img/ico-3.svg" alt="">
                <div class="d-flex flex-column"><span class="info__nums"><?=$arParams['CARS']?> <?=Loc::getMessage('THING')?>.</span><span class="info__desc"><?=Loc::getMessage('INFO_CARS_MES')?></span></div>
            </div>
            <div class="w-100 d-none d-xl-block"></div>
            <div class="col-6 col-xl-3 info__item d-flex d-md-block flex-column flex-md-row"><img class="ico" src="<?=SITE_TEMPLATE_PATH?>/img/ico-4.svg" alt="">
                <div class="d-flex flex-column"><span class="info__nums"><?=$arParams['APARTMENT']?> <?=Loc::getMessage('THING')?>.</span><span class="info__desc"><?=Loc::getMessage('INFO_APARTMENT_MES')?></span></div>
            </div>
            <div class="col-6 col-xl-3 info__item d-flex d-md-block flex-column flex-md-row"><img class="ico" src="<?=SITE_TEMPLATE_PATH?>/img/ico-5.svg" alt="">
                <div class="d-flex flex-column"><span class="info__nums"><?=$arParams['AREA']?> <?=Loc::getMessage('METERS')?><sup>2</sup></span><span class="info__desc"><?=Loc::getMessage('INFO_AREA_MES')?></span></div>
            </div>
            <div class="col-6 col-xl-3 info__item d-flex d-md-block flex-column flex-md-row"><img class="ico" src="<?=SITE_TEMPLATE_PATH?>/img/ico-6.svg" alt="">
                <div class="d-flex flex-column"><span class="info__nums"><?=$arParams['PEOPLE']?> <?=Loc::getMessage('HUMANS')?>.</span><span class="info__desc"><?=Loc::getMessage('INFO_PEOPLE_MES')?></span></div>
            </div>
        </div>
        <button class="button button--large" type="undefined"><?=Loc::getMessage('SIGN_UP')?></button>
    </div>
</div>
