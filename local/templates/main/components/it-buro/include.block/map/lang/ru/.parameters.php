<?php

$MESS['MAP_COMPLEX_ADDRESS'] = 'Адрес комплекса';
$MESS['MAP_OFFICE_ADDRESS'] = 'Адрес офиса';
$MESS['MAP_OFFICE_PHONE'] = 'Телефон офиса';
$MESS['MAP_OFFICE_LAT'] = 'Координаты офиса (широта)';
$MESS['MAP_OFFICE_LONG'] = 'Координаты офиса (долгота)';
$MESS['MAP_COMPLEX_LAT'] = 'Координаты комплекса (широта)';
$MESS['MAP_COMPLEX_LONG'] = 'Координаты комплекса (долгота)';
$MESS['MAP_COMPLEX_BALOON_TEXT'] = 'Текст балуна для комплекса';
$MESS['MAP_OFFICE_BALOON_TEXT'] = 'Текст балуна для офиса';