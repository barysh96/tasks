<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

$arTemplateParameters = [
    'COMPLEX_ADDRESS' => [
        'NAME' => Loc::getMessage('MAP_COMPLEX_ADDRESS'),
        'TYPE' => 'STRING',
    ],
    'OFFICE_ADDRESS' => [
        'NAME' => Loc::getMessage('MAP_OFFICE_ADDRESS'),
        'TYPE' => 'STRING',
    ],
    'OFFICE_PHONE' => [
        'NAME' => Loc::getMessage('MAP_OFFICE_PHONE'),
        'TYPE' => 'STRING',
    ],
    'OFFICE_LAT' => [
        'NAME' => Loc::getMessage('MAP_OFFICE_LAT'),
        'TYPE' => 'STRING',
    ],
    'OFFICE_LONG' => [
        'NAME' => Loc::getMessage('MAP_OFFICE_LONG'),
        'TYPE' => 'STRING',
    ],
    'COMPLEX_LAT' => [
        'NAME' => Loc::getMessage('MAP_COMPLEX_LAT'),
        'TYPE' => 'STRING',
    ],
    'COMPLEX_LONG' => [
        'NAME' => Loc::getMessage('MAP_COMPLEX_LONG'),
        'TYPE' => 'STRING',
    ],
    'COMPLEX_BALOON_TEXT' => [
        'NAME' => Loc::getMessage('MAP_COMPLEX_BALOON_TEXT'),
        'TYPE' => 'STRING',
    ],
    'OFFICE_BALOON_TEXT' => [
        'NAME' => Loc::getMessage('MAP_OFFICE_BALOON_TEXT'),
        'TYPE' => 'STRING',
    ],
];
