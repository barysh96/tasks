<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

use \Bitrix\Main\Localization\Loc;
?>
<div class="block-map d-none d-xl-block" id="block-map">
    <div class="container d-xl-flex justify-content-between d-md-block">
        <div class="block-map__places col-md-12 col-xl-3">
            <div class="block-map__buid">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/block-map-ico-1.svg" alt="">
                <div class="block-map__places-content d-flex flex-column">
                    <h3><?=Loc::getMessage('MAP_COMPLEX')?></h3>
                    <div class="block-map__desc text--white">
                        <?=$arParams['COMPLEX_ADDRESS']?>
                    </div>
                </div>
            </div>
            <div class="block-map__office">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/block-map-ico-2.svg" alt="">
                <div class="block-map__places-content d-flex flex-column">
                    <h3><?=Loc::getMessage('MAP_OFFICE')?></h3>
                    <span class="block-map__phone text--white"><?=$arParams['OFFICE_PHONE']?></span>
                    <div class="block-map__desc text--white"><?=$arParams['OFFICE_ADDRESS']?></div>
                </div>
            </div>
        </div>
        <div class="block-map__map col-lg-12 col-xl-9" id="map"></div>
    </div>
</div>
<script type="text/javascript">
    BX.message({
        OFFICE_LAT : "<?=$arParams['OFFICE_LAT']?>",
        OFFICE_LONG : "<?=$arParams['OFFICE_LONG']?>",
        OFFICE_TEXT : "<?=$arParams['OFFICE_BALOON_TEXT']?>",
        COMPLEX_LAT : "<?=$arParams['COMPLEX_LAT']?>",
        COMPLEX_LONG : "<?=$arParams['COMPLEX_LONG']?>",
        COMPLEX_TEXT : "<?=$arParams['COMPLEX_BALOON_TEXT']?>",
        SITE_TEMPLATE_PATH: "<?=SITE_TEMPLATE_PATH?>"
    });
</script>
