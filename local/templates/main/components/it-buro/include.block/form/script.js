$(function () {
    $(document).on('submit', '#sign_in_form', function (e) {
        e.preventDefault();
        let form = $(this);
        let formData = new FormData($(form)[0]);
        $.ajax({
            url: form.attr('action'),
            method: 'POST',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.success === true) {
                    form.prevAll('.order__title').text(BX.message('SUCCESS_TEXT'));
                    form.trigger('reset');
                } else if (response.error === true) {
                    form.prevAll('.order__title').text(BX.message('ERROR_TEXT')).append('\n'+response.text);
                }
            }
        });
    });

    $(document).on('click', '#file_trigger', function (e) {
        e.preventDefault();
        $('#form_file_input').trigger('click');
    });

    $(document).on('change', '#form_file_input', function() {
        $('#file_trigger').text(this.files[0].name);
    });
});