<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

$arTemplateParameters = [
    'AGREEMENT_LINK' => [
        'NAME' => Loc::getMessage('FORM_AGREEMENT_LINK'),
        'TYPE' => 'STRING',
    ],
    'TITLE' => [
        'NAME' => Loc::getMessage('FORM_TITLE'),
        'TYPE' => 'STRING',
    ],
    'FORM_IBLOCK_ID' => [
        'NAME' => Loc::getMessage('FORM_FORM_IBLOCK_ID'),
        'TYPE' => 'STRING',
    ],
];
