<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

use \Bitrix\Main\Localization\Loc;
?>
<div class="sign d-none d-xl-block">
    <div class="container">
        <div class="order__title title text--white col-10 offset-0 offset-lg-1"><?=$arParams['TITLE']?></div>
        <form class="form d-flex flex-wrap" enctype="multipart/form-data" id="sign_in_form" action="<?=$this->getFolder();?>/ajax.php" method="post">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="FORM_IBLOCK_ID" value="<?=$arParams['FORM_IBLOCK_ID']?>">
            <div class="col-12 offset-0 col-lg-5 offset-lg-1">
                <div class="form-group">
                    <label>
                        <input class="form-control" name="NAME" required type="text" placeholder="<?=Loc::getMessage('FORM_PLACEHOLDER_NAME')?>">
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <input class="form-control" name="PHONE" required type="text" placeholder="<?=Loc::getMessage('FORM_PLACEHOLDER_PHONE')?>">
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <input class="form-control" name="EMAIL" required type="email" placeholder="<?=Loc::getMessage('FORM_PLACEHOLDER_EMAIL')?>">
                    </label>
                </div>
                <div class="form-group">
                    <button class="button button--standart" id="file_trigger"><?=Loc::getMessage('FORM_FILE_INPUT')?></button>
                    <input type="file" name="FILE" class="form-control-file" id="form_file_input">
                </div>
            </div>
            <div class="form__content col-12 col-lg-5">
                <label>
                    <textarea class="form-control" name="TEXT" required placeholder="<?=Loc::getMessage('FORM_PLACEHOLDER_TEXT')?>"></textarea>
                </label>
                <p class="form__help text--white"><?=Loc::getMessage('FORM_REQ')?></p>
            </div>
            <div class="form__send d-flex align-items-center flex-column col-12 offset-0 col-md-10 flex-md-row offset-md-1">
                <button class="button button--standart" type="submit"><?=Loc::getMessage('FORM_SEND')?></button>
                <div class="form__privacy text--white">
                    <?=Loc::getMessage('FORM_AGREEMENT', ['#HREF#' => $arParams['AGREEMENT_LINK']])?>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    BX.message({
        SUCCESS_TEXT: '<?=Loc::getMessage('SUCCESS_TEXT')?>',
        ERROR_TEXT: '<?=Loc::getMessage('ERROR_TEXT')?>'
    });
</script>
