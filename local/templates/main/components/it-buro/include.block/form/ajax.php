<?php
require($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/main/include/prolog_before.php');

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
if ($request->isPost() && check_bitrix_sessid()) {
    \Bitrix\Main\Loader::includeModule('iblock');
    $cIblockElement = new \CIBlockElement();
    $resId = $cIblockElement->Add([
        'IBLOCK_ID' => $request->getPost('FORM_IBLOCK_ID'),
        'NAME' => $request->getPost('NAME'),
        'PREVIEW_TEXT' => $request->getPost('TEXT'),
        'PROPERTY_VALUES' => [
            'PHONE' => $request->getPost('PHONE'),
            'EMAIL' => $request->getPost('EMAIL'),
            'FILE' => $request->getFile('FILE')
        ]
    ]);
    if ($resId) {
        $result = ['success' => true];
    } else {
        $result = ['error' => true, 'text' => $cIblockElement->LAST_ERROR];
    }
    echo \Bitrix\Main\Web\Json::encode($result);
}
die;
