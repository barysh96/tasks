<?php

$MESS['FORM_PLACEHOLDER_NAME'] = 'Имя';
$MESS['FORM_PLACEHOLDER_PHONE'] = 'Телефон';
$MESS['FORM_PLACEHOLDER_EMAIL'] = 'Email для ответа';
$MESS['FORM_PLACEHOLDER_TEXT'] = 'Ваши вопросы';
$MESS['FORM_REQ'] = 'Все поля обязательны для заполнения';
$MESS['FORM_SEND'] = 'Отправить';
$MESS['SUCCESS_TEXT'] = 'Форма успешно отправлена!';
$MESS['ERROR_TEXT'] = 'При отправке произошла ошибка';
$MESS['FORM_AGREEMENT'] = 'Нажимая «Отправить», я даю согласие <a class="form__privacy-link text--white" href="#HREF#">на обработку персональных данных</a>';
$MESS['FORM_FILE_INPUT'] = 'Прикрепить файл';