<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
?>
<div class="location d-lg-flex">
    <div class="location__about col-12 col-lg-5 order-lg-1">
        <div class="title title--gold"><?=$arParams['TITLE']?></div>
        <?php $APPLICATION->IncludeComponent("bitrix:main.include", ".default",
            array(
                "COMPONENT_TEMPLATE" => ".default",
                "PATH" => $arParams['TEXT_PATH'],
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "",
                "AREA_FILE_RECURSIVE" => "Y",
                "EDIT_TEMPLATE" => "standard.php"
            ),
            false
        );
        ?>
    </div>
    <div class="location__video col-lg-6 d-flex justify-content-lg-center">
        <div class="player col-xl-10">
            <div class="player">
                <div class="player__wrapper">
                    <img class="player__img" src="<?=$arParams['PICTURE']?>">
                    <a class="popup-youtube" href="https://www.youtube.com/watch?v=<?=$arParams['VIDEO_CODE']?>?autoplay=1&amp;rel=0">
                        <img class="player__play" src="<?=SITE_TEMPLATE_PATH?>/img/play-button.svg">
                    </a>
                </div>
            </div>
            <div class="signature"><?=$arParams['VIDEO_DESCRIPTION']?></div>
        </div>
    </div>
</div>
