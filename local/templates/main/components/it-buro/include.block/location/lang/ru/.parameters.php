<?php

$MESS['LOCATION_TITLE'] = 'Заголовок';
$MESS['LOCATION_PICTURE'] = 'Превью для видео';
$MESS['LOCATION_VIDEO_CODE'] = 'Код видео из YouTube';
$MESS['LOCATION_VIDEO_DESCRIPTION'] = 'Описание видео';
$MESS['LOCATION_TEXT_PATH'] = 'Путь к включаемой области текста';