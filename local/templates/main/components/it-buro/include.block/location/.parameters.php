<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

$arTemplateParameters = [
    'TITLE' => [
        'NAME' => Loc::getMessage('LOCATION_TITLE'),
        'TYPE' => 'STRING',
    ],
    'PICTURE' => [
        'NAME' => Loc::getMessage('LOCATION_PICTURE'),
        "PARENT" => "BASE",
        "TYPE" => "FILE",
        "FD_TARGET" => "F",
        "FD_EXT" => "png,gif,jpg,jpeg",
        "FD_UPLOAD" => true,
        "FD_USE_MEDIALIB" => true,
        "FD_MEDIALIB_TYPES" => [
            'image',
        ],
    ],
    'VIDEO_CODE' => [
        'NAME' => Loc::getMessage('LOCATION_VIDEO_CODE'),
        'TYPE' => 'STRING',
    ],
    'VIDEO_DESCRIPTION' => [
        'NAME' => Loc::getMessage('LOCATION_VIDEO_DESCRIPTION'),
        'TYPE' => 'STRING',
    ],
    'TEXT_PATH' => [
        'NAME' => Loc::getMessage('LOCATION_TEXT_PATH'),
        'TYPE' => 'STRING',
    ]
];
