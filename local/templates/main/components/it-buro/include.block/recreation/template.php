<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
?>
<div class="recreation d-none d-xl-block">
    <div class="recreation__content d-flex flex-column flex-lg-row">
        <div class="recreation__img col-lg-6" style="background-image: url('<?=$arParams['PICTURE']?>')"></div>
        <div class="recreation__about col-lg-5">
            <div class="title title--gold"><?=$arParams['TITLE']?></div>
            <div class="text text--white">
                <?php $APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                    array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => $arParams['TEXT_PATH'],
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "",
                        "AREA_FILE_RECURSIVE" => "Y",
                        "EDIT_TEMPLATE" => "standard.php"
                    ),
                    false
                );
                ?>
            </div>
        </div>
    </div>
</div>
