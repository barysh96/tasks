<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

$arTemplateParameters = [
    'TITLE' => [
        'NAME' => Loc::getMessage('RECREATION_TITLE'),
        'TYPE' => 'STRING',
    ],
    'PICTURE' => [
        'NAME' => Loc::getMessage('RECREATION_PICTURE'),
        "PARENT" => "BASE",
        "TYPE" => "FILE",
        "FD_TARGET" => "F",
        "FD_EXT" => "png,gif,jpg,jpeg",
        "FD_UPLOAD" => true,
        "FD_USE_MEDIALIB" => true,
        "FD_MEDIALIB_TYPES" => [
            'image',
        ],
    ],
    'TEXT_PATH' => [
        'NAME' => Loc::getMessage('RECREATION_TEXT_PATH'),
        'TYPE' => 'STRING',
    ]
];
