<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

if (!empty($arResult)):?>
    <nav class="nav align-items-start justify-content-xl-end align-items-xl-center justify-content-center h-100">
        <ul class="nav__list d-flex flex-column align-center justify-content-around flex-xl-row h-100">
            <?php
            foreach ($arResult as $arItem):
                if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) {
                    continue;
                }
            ?>
                <li class="nav__item <?if ($arItem["SELECTED"]):?> nav__item--active <?endif;?>">
                    <a class="nav__link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                </li>
            <?php
            endforeach;?>
        </ul>
    </nav>
<?php
endif;?>