<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

if (!empty($arResult)):?>
    <div class="header__links d-md-block">
    <?php
    foreach ($arResult as $index => $arItem):
        if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) {
            continue;
        }

        $comma = $index == count($arResult) - 1 ? '' : ',';
    ?>
        <a class="header__links--item" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a><?=$comma?>
    <?php
    endforeach;?>

    </div>
<?php
endif;?>