<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

if (!empty($arResult['ITEMS'])) {
    foreach ($arResult['ITEMS'] as $index => $arItem) {
        if (!empty($pictureId = $arItem['PREVIEW_PICTURE']['ID'])) {
            $arResult['ITEMS'][$index]['PREVIEW_PICTURE']['SRC'] = \CFile::ResizeImageGet(
                $pictureId,
                [
                    'width' => 238,
                    'height' => 110
                ],
                BX_RESIZE_IMAGE_PROPORTIONAL_ALT
            )['src'];
        }
    }
}