<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="near__picture d-lg-flex col-lg-6 align-items-lg-center justify-content-lg-center">
    <div class="near__picture-items d-flex flex-wrap align-items-center justify-content-center">
        <?foreach ($arResult['ITEMS'] as $arItem):?>
            <div class="near__img"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt=""></div>
        <?endforeach;?>
    </div>
</div>
