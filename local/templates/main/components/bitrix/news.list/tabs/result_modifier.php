<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

if (!empty($arResult['ITEMS'])) {
    $strEntityDataClass = false;
    foreach ($arResult['ITEMS'] as $index => $arItem) {
        if (!empty($arItem['PROPERTIES']['APARTMENTS']['VALUE'])) {
            if (!$strEntityDataClass) {
                \Bitrix\Main\Loader::includeModule('highloadblock');
                $arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getRow([
                    'filter' => [
                        '=TABLE_NAME' => $arItem['PROPERTIES']['APARTMENTS']['USER_TYPE_SETTINGS']['TABLE_NAME']
                    ]
                ]);
                $obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
                $strEntityDataClass = $obEntity->getDataClass();
            }
            $arResult['ITEMS'][$index]['APARTMENTS'] = $strEntityDataClass::getList([
                'filter' => [
                    '=UF_XML_ID' => $arItem['PROPERTIES']['APARTMENTS']['VALUE']
                ]
            ])->fetchAll();
        }
    }
}