<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;
?>
<div class="project">
    <div class="container">
        <ul class="nav nav-tabs project__switch d-flex" id="myTab" role="tablist">
            <?foreach ($arResult['ITEMS'] as $index => $arItem):?>
                <li class="nav-item">
                    <a class="nav-link <?if (!$index) echo 'active';?> project__switch-item" id="<?=$arItem['CODE']?>-tab" data-toggle="tab" href="#<?=$arItem['CODE']?>" role="tab" aria-controls="<?=$arItem['CODE']?>" aria-selected="true"><?=$arItem['NAME']?></a>
                </li>
            <?endforeach;?>
        </ul>
    </div>
    <div class="tab-content" id="myTabContent">
        <?foreach ($arResult['ITEMS'] as $index => $arItem):?>
            <div class="project__content tab-pane fade <?if (!$index) echo 'show active';?>" id="<?=$arItem['CODE']?>" role="tabpanel" aria-labelledby="<?=$arItem['CODE']?>-tab">
                <div class="project__wrap container-fluid d-flex">
                    <div class="row">
                        <div class="project__picture col-12 col-lg-6 order-lg-1" style="background-image: url('<?=$arItem['PREVIEW_PICTURE']['SRC']?>')"></div>
                        <div class="container-fluid col-12 col-lg-6">
                            <div class="project__about">
                                <div class="title title--gold project__title"><?=$arItem['NAME']?></div>
                                <div class="text text--white project__text">
                                    <?=$arItem['PREVIEW_TEXT']?>
                                </div>
                                <?if ($arItem['APARTMENTS']):?>
                                    <table class="project__parametrs">
                                        <tr class="first-line">
                                            <th><?=Loc::getMessage('APARTMENTS_TABS')?></th>
                                            <th><?=Loc::getMessage('AREA_TABS')?></th>
                                            <th><?=Loc::getMessage('PRICE_TABS')?></th>
                                        </tr>
                                        <?foreach ($arItem['APARTMENTS'] as $apartment):?>
                                            <tr>
                                                <td class="name-col"><?=$apartment['UF_NAME']?></td>
                                                <td class="square"><?=Loc::getMessage('AREA_TABS')?></td>
                                                <td class="square__num"><?=$apartment['UF_AREA']?> <?=Loc::getMessage('METERS_TABS')?><sup>2</sup></td>
                                                <td class="price"><?=Loc::getMessage('PRICE_TABS')?></td>
                                                <td class="price__num"><?=$apartment['UF_PRICE']?></td>
                                            </tr>
                                        <?endforeach;?>
                                    </table>
                                <?endif;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?endforeach;?>
    </div>
</div>
