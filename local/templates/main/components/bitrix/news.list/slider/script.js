$(function () {
    $('.slider__main').slick({
        arrows: false,
        fade: true,
        asNavFor: '.slider__dot'
    });

    $('.slider__dot').slick({
        slidesToShow: BX.message('SLIDER_COUNT'),
        arrows: false,
        focusOnSelect: true,
        asNavFor: '.slider__main'
    });
});