<?php

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

use \Bitrix\Main\Page\Asset;
global $APPLICATION;
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta charset="<?=SITE_CHARSET?>">
        <meta name="robots" content="index, follow">
        <?php
        Asset::getInstance()->addCss('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css');
        Asset::getInstance()->addCss('https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css');
        Asset::getInstance()->addCss('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/main.css');

        Asset::getInstance()->addJs('https://code.jquery.com/jquery-3.4.1.min.js');
        Asset::getInstance()->addJs('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js');
        Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js');
        Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js');
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/main.js');

        CJSCore::Init();
        ?>
        <title><?$APPLICATION->ShowTitle();?></title>
        <script src="https://api-maps.yandex.ru/2.1/?apikey=&lt;e1f2c454-88cf-4d81-b25f-09909cd64251&gt;&amp;lang=ru_RU" type="text/javascript"></script>
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <?$APPLICATION->ShowHead();?>
    </head>
    <body>
        <?$APPLICATION->ShowPanel();?>
        <header class="header">
            <div class="container header__container">
                <div class="row align-items-center h-100">
                    <div class="col-10 col-xl-4 d-flex">
                        <a class="logo" href="/">
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                                array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "PATH" => SITE_DIR."include/header/logo.php",
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "",
                                    "AREA_FILE_RECURSIVE" => "Y",
                                    "EDIT_TEMPLATE" => "standard.php"
                                ),
                                false
                            );
                            ?>
                        </a>
                        <?$APPLICATION->IncludeComponent("bitrix:menu", "header_left",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                                "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                                "DELAY" => "N",	// Откладывать выполнение шаблона меню
                                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                                "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                                    0 => "",
                                ),
                                "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                                "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                                "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                                "ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
                                "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                            ),
                            false
                        );
                        ?>
                    </div>
                    <div class="col-xl-8 nav-mobile">
                        <?$APPLICATION->IncludeComponent("bitrix:menu", "header_right",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                                "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                                "DELAY" => "N",	// Откладывать выполнение шаблона меню
                                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                                "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                                    0 => "",
                                ),
                                "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                                "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                                "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                                "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                                "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                            ),
                            false
                        );
                        ?>
                    </div>
                    <div class="col-2 d-flex d-xl-none justify-content-end">
                        <a class="burger-menu">
                            <span class="burger-menu__elem"></span>
                            <span class="burger-menu__elem"></span>
                            <span class="burger-menu__elem"></span>
                        </a>
                    </div>
                </div>
            </div>
        </header>