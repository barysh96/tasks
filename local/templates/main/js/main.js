function scrollToFunc(objectThis) {
    var point = objectThis.attr('href');
    var top = $(point).offset().top;
    $('body, html').animate({
        scrollTop: top
    },800);
}

$(document).ready(function() {
    $('.popup-youtube').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        preloader: true,
    });

    $(this).on('click', '.burger-menu', function () {
        $(this).toggleClass('show');
        $('.nav-mobile').toggleClass('show');
        $('body').toggleClass('menu-open');
    })
    .on('click', '.look-map', function(event) {
        event.preventDefault();
        scrollToFunc($(this));
    });
});